#!/bin/bash

dir="."
echo "$dir/model"
if [ ! -d "$dir/model" ]
then
  mkdir -p "$dir/model/ivector"
  mkdir -p "$dir/model/conf"
  mkdir -p "$dir/model/am"
  mkdir -p "$dir/model/graph/phones"

fi
# 
cp exp/nnet3/extractor/final.dubm "$dir/model/ivector"
cp exp/nnet3/extractor/final.ie "$dir/model/ivector"
cp exp/nnet3/extractor/final.mat "$dir/model/ivector"
cp exp/nnet3/extractor/global_cmvn.stats "$dir/model/ivector"
cp exp/nnet3/extractor/online_cmvn.conf "$dir/model/ivector"
echo "--left-context=3 
--right-context=3" > "$dir/model/ivector/splice.conf"

cp conf/mfcc_hires.conf "$dir/model/conf/mfcc.conf"
cp exp/chain/tdnn1*_sp/final.mdl "$dir/model/am"
cp exp/chain/tree_*_sp/graph/HCLG.fst "$dir/model/graph"
cp exp/chain/tree_*_sp/graph/phones.txt "$dir/model/graph"
cp exp/chain/tree_*_sp/graph/words.txt "$dir/model/graph"
cp exp/chain/tree_*_sp/graph/phones/word_boundary.int "$dir/model/graph/phones" 

echo "--min-active=200
--max-active=3000
--beam=10.0
--lattice-beam=2.0
--acoustic-scale=1.0
--frame-subsampling-factor=3
--endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10
--endpoint.rule2.min-trailing-silence=0.5
--endpoint.rule3.min-trailing-silence=1.0
--endpoint.rule4.min-trailing-silence=2.0" > "$dir/model/conf/model.conf"
